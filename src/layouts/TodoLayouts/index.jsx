export const TodoLayouts = ({ children }) => {
  <div className="container">{children}</div>;
};

export default TodoLayouts;
