import { SET_THEME } from '@containers/Theme/constants';

export function setTheme(theme) {
  return {
    type: SET_THEME,
    theme,
  };
}
