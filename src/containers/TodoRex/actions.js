import { SET_TODO, UPDATE_TODO } from '@containers/TodoRex/constants';

export function setTodo(todoList) {
  return {
    type: SET_TODO,
    todoList,
  };
}

export const updateTodo = (todoList) => ({
  type: UPDATE_TODO,
  payload: todoList,
});
