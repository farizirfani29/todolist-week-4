/* eslint-disable default-param-last */
import { produce } from 'immer';
import { SET_TODO, UPDATE_TODO } from '@containers/TodoRex/constants';

export const initialState = {
  todoList: [],
};

export const storedKey = ['todoList'];

const todoReducer = (state = initialState, action) =>
  produce(state, (draft) => {
    switch (action.type) {
      case SET_TODO:
        draft.todoList = [...draft.todoList, action.todoList];
        break;
      case UPDATE_TODO:
        draft.todoList = action.payload;
        break;
      default:
        break;
    }
  });

export default todoReducer;
