import { createSelector } from 'reselect';
import { initialState } from '@containers/TodoRex/reducer';

const selectTodoState = (state) => state.todoList || initialState;

const selectTodo = createSelector(selectTodoState, (state) => state);

export { selectTodo };
