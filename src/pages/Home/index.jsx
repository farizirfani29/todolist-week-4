import { FormattedMessage } from 'react-intl';

const Home = () => (
  <div>
    <FormattedMessage id="app_greeting" />
    <h1>testing</h1>
  </div>
);

export default Home;
