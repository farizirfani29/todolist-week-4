/* eslint-disable no-shadow */
/* eslint-disable jsx-a11y/label-has-associated-control */
/* eslint-disable jsx-a11y/no-noninteractive-element-interactions */
/* eslint-disable react/no-unused-prop-types */

// import from package img etc.
import uuid from 'react-uuid';
import Swal from 'sweetalert2';
import PropTypes from 'prop-types';
import { DragDropContext, Droppable, Draggable } from 'react-beautiful-dnd';
import imgLight from '@static/images/bg-desktop-light.jpg';
import imgDark from '@static/images/bg-desktop-dark.jpg';
import iconSun from '@static/images/icon-sun.svg';
import iconMoon from '@static/images/icon-moon.svg';
import iconCross from '@static/images/icon-cross.svg';
import iconEdit from '@static/images/icon-edit.svg';

// import from component
import { useDispatch, useSelector } from 'react-redux';
import { setTheme } from '@containers/Theme/actions';
import { setTodo, updateTodo } from '@containers/TodoRex/actions';
import { useState, useMemo } from 'react';

// import from style

import '../../styles/sweetalert.scss';
import classes from './style.module.scss';

const Todo = () => {
  const dispatch = useDispatch();
  const theme = useSelector((state) => state.theme.theme);
  const Todos = useSelector((state) => state.todoList.todoList);
  const [filter, setFilter] = useState('all');
  const [editingTodo, setEditingTodo] = useState('');
  const [selectedId, setSelectedId] = useState('');

  // ============================ for edit ===========================

  const handleEdit = (id, title) => {
    // const todoToEdit = Todos.find((todo) => todo.id === id);
    setEditingTodo(title);
    setSelectedId(id);
  };

  // =========================== for Clear Complated ===========================

  const handleClearCompleted = () => {
    dispatch(updateTodo(Todos.filter((item) => !item.completed)));
  };

  // =========================== for checkbox ===========================
  const handleCheckboxChange = (id) => {
    const updatedTodos = Todos.map((item) => (item.id === id ? { ...item, completed: !item.completed } : item));
    dispatch(updateTodo(updatedTodos));
  };

  const handleStyleText = (isActivated) => ({
    textDecoration: isActivated ? 'line-through' : 'none',
  });

  // =========================== for filter ===========================
  const handleFilterChange = (selectedFilter) => {
    setFilter(selectedFilter);
  };

  // =========================== for filter ===========================
  const filteredTodos = useMemo(
    () =>
      Todos.filter((item) => {
        if (filter === 'all') {
          return true;
        }
        if (filter === 'active') {
          return !item.completed;
        }
        if (filter === 'completed') {
          return item.completed;
        }
      }),
    [filter, Todos]
  );

  const movePositionById = (sourceId, destinationId, array) => {
    const sourceIndex = array.findIndex((item) => item.id === sourceId);
    const destinationIndex = array.findIndex((item) => item.id === destinationId);

    if (sourceIndex === -1 || destinationIndex === -1) {
      return array;
    }

    const newArray = [...array];
    const [removedItem] = newArray.splice(sourceIndex, 1);
    newArray.splice(destinationIndex, 0, removedItem);
    return newArray;
  };

  // ============================ dnd ================================
  const onDragEnd = (result) => {
    if (!result.destination) {
      return;
    }
    const sourceId = filteredTodos[result.source.index].id;
    const destinationId = filteredTodos[result.destination.index].id;
    const moveData = movePositionById(sourceId, destinationId, Todos);
    dispatch(updateTodo(moveData));
  };

  // =========================== for create new todo ===========================

  const handleKeyDown = (event) => {
    const value = event.target.value.trim();
    // const { key } = event;
    if (event.key === 'Enter' && value !== '') {
      const newTodo = {
        id: uuid(),
        title: value,
        completed: false,
      };
      if (selectedId) {
        const updatedTodos = Todos.map((todo) => {
          if (todo.id === selectedId) {
            return { ...todo, title: value };
          }
          return todo;
        });
        dispatch(updateTodo(updatedTodos));
      } else {
        dispatch(setTodo(newTodo));
      }
      setEditingTodo('');
      setSelectedId('');
    }
  };

  // ========================== for change theme ===========================
  const onSwitch = () => {
    const newTheme = theme === 'dark' ? 'light' : 'dark';
    dispatch(setTheme(newTheme));
  };
  const isDark = theme === 'dark';
  const ligthThemeStyle = {
    backgroundColor: '#fefeff',
    bgLight2: 'hsl(0, 0%, 100%)',
    color: '#60637a',
    textLight2: 'hsl(0, 0%, 52%)',
    borderRadius: '5px',
  };
  const darkThemeStyle = {
    backgroundColor: '#24273d',
    bgDark2: 'hsl(209, 23%, 22%)',
    color: '#bebfc7',
    colorSec: 'hsl(0, 0%, 52%)',
    borderRadius: '5px',
  };
  const setStyle = isDark ? darkThemeStyle : ligthThemeStyle;

  // =========================== for delete todo ===========================

  const handleDelete = (id) => {
    Swal.fire({
      title: 'Konfirmasi',
      text: 'Apakah Anda yakin ingin menghapus data ini?',
      icon: 'error',
      showCancelButton: true,
      confirmButtonText: 'Ya',
      cancelButtonText: 'Batal',
      background: isDark ? '' : '#fafbfb',
      color: isDark ? '#fbfbfb' : '#4b5563',
      confirmButtonColor: isDark ? '#3085d6' : '#3085d6',
      cancelButtonColor: isDark ? '#d33' : '#d33',
      customClass: {
        container: isDark ? 'swal2-theme-dark' : 'swal2-theme-light',
      },
    }).then((result) => {
      if (result.value) {
        const updatedTodos = Todos.filter((todo) => todo.id !== id);
        dispatch(updateTodo(updatedTodos));
      }
    });
  };

  return (
    <div className={classes.container} style={isDark ? { backgroundColor: '#161721' } : { backgroundColor: '#fafbfb' }}>
      <img className={classes.image} src={isDark ? imgDark : imgLight} alt="" />
      <div className={classes.content}>
        <div className={classes.box_content}>
          <div className={classes.header}>
            <h1>TODO</h1>
            <div onClick={onSwitch}>
              <img draggable="false" style={{ pointerEvents: 'none' }} src={isDark ? iconSun : iconMoon} alt="" />
            </div>
          </div>
          <div className={classes.input_box} style={setStyle}>
            <input
              type="text"
              onChange={(e) => setEditingTodo(e.target.value)}
              value={editingTodo}
              onKeyDown={handleKeyDown}
              className={classes.input}
              placeholder="Enter for new List"
              style={setStyle}
            />
            {/* <button type="button" onKeyDown={handleKeyDown}>
              click
            </button> */}
          </div>
          {/* ======================================== */}
          <div className={classes.list_box} style={setStyle}>
            <div className={classes.scroll}>
              <DragDropContext onDragEnd={onDragEnd}>
                <Droppable droppableId="droppable">
                  {(provided) => (
                    <div ref={provided.innerRef} {...provided.droppableProps}>
                      {filteredTodos.length === 0 ? (
                        <p style={{ textAlign: 'center' }}>Task Not Found</p>
                      ) : (
                        filteredTodos.map((item, index) => (
                          <Draggable key={item.id} draggableId={item.id} index={index}>
                            {(provided) => (
                              <div
                                ref={provided.innerRef}
                                {...provided.draggableProps}
                                {...provided.dragHandleProps}
                                className={classes.list}
                              >
                                <input
                                  defaultChecked={item.completed}
                                  onClick={() => handleCheckboxChange(item.id)}
                                  type="checkbox"
                                  className={classes.custom_checkbox}
                                  name="list"
                                  id="myCheckbox"
                                />
                                <p style={handleStyleText(item.completed)}>{item.title}</p>
                                <div style={{ cursor: 'pointer', marginLeft: 'auto', marginRight: '10px' }}>
                                  <div className={classes.icons}>
                                    <img src={iconEdit} onClick={() => handleEdit(item.id, item.title)} alt="" />
                                    <img src={iconCross} onClick={() => handleDelete(item.id)} alt="" />
                                  </div>
                                </div>
                              </div>
                            )}
                          </Draggable>
                        ))
                      )}
                      {provided.placeholder}
                    </div>
                  )}
                </Droppable>
              </DragDropContext>
            </div>
            <div className={classes.utils_box}>
              <p>{filteredTodos.length} items left</p>
              <div className={classes.utils_link}>
                <button
                  type="button"
                  onClick={() => handleFilterChange('all')}
                  style={setStyle && { cursor: 'pointer', color: 'hsl(220, 98%, 61%)', fontWeight: 'bold' }}
                >
                  All
                </button>
                <button
                  type="button"
                  onClick={() => handleFilterChange('active')}
                  style={setStyle && { cursor: 'pointer' }}
                >
                  Active
                </button>
                <button
                  type="button"
                  onClick={() => handleFilterChange('completed')}
                  style={setStyle && { cursor: 'pointer' }}
                >
                  Complated
                </button>
              </div>
              <p onClick={handleClearCompleted} style={setStyle && { cursor: 'pointer', colorSec: 'blue' }}>
                Clear Complated
              </p>
            </div>
          </div>
          <div className={classes.utils_link_mobile} style={setStyle}>
            <button
              type="button"
              onClick={() => handleFilterChange('all')}
              style={setStyle && { cursor: 'pointer', color: 'hsl(220, 98%, 61%)' }}
            >
              All
            </button>
            <button
              type="button"
              onClick={() => handleFilterChange('active')}
              style={setStyle && { cursor: 'pointer' }}
            >
              Active
            </button>
            <button
              type="button"
              onClick={() => handleFilterChange('completed')}
              style={setStyle && { cursor: 'pointer' }}
            >
              Complated
            </button>
          </div>
          <p style={isDark ? { color: 'white', fontSize: '16px' } : { color: 'hsl(200, 15%, 8%)', fontSize: '16px' }}>
            Drag and drop to reander list
          </p>
        </div>
      </div>
    </div>
  );
};

Todo.prototype = {
  theme: PropTypes.string,
};

export default Todo;
