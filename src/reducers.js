import { combineReducers } from 'redux';

import appReducer from '@containers/App/reducer';
import languageReducer, { storedKey as storedLangState } from '@containers/Language/reducer';
import themeReducer, { storedKey as storedTheme } from '@containers/Theme/reducer';
import todoReducer, { storedKey as stroredTodo } from '@containers/TodoRex/reducer';

import { mapWithPersistor } from './persistence';

// console.log(themeReducer, '======theme');
// console.log(todoReducer, '======todo');

const storedReducers = {
  language: { reducer: languageReducer, whitelist: storedLangState },
  theme: { reducer: themeReducer, whitelist: storedTheme },
  todoList: { reducer: todoReducer, whitelist: stroredTodo },
};

const temporaryReducers = {
  app: appReducer,
};

const createReducer = () => {
  const coreReducer = combineReducers({
    ...mapWithPersistor(storedReducers),
    ...temporaryReducers,
  });
  const rootReducer = (state, action) => coreReducer(state, action);
  return rootReducer;
};

export default createReducer;
